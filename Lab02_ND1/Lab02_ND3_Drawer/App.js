/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React, {useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import CalculationScreen from './screens/CalculationScreen';
import EquationScreen from './screens/EquationScreen';
import AboutScreen from './screens/AboutScreen';
import EquationScreenPrompt from './screens/EquationScreenPrompt';
import {createDrawerNavigator} from '@react-navigation/drawer';

const Drawer = createDrawerNavigator();
const App = () => {
  return (
    <NavigationContainer>
      <Drawer.Navigator>
        <Drawer.Screen name="Calculation" component={CalculationScreen} />
        <Drawer.Screen name="Equation" component={EquationScreen} />
        {/* <Drawer.Screen name="Equation" component={EquationScreenPrompt} /> */}
        <Drawer.Screen name="About" component={AboutScreen} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
};
export default App;
