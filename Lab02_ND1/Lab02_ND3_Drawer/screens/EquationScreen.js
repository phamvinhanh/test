/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React, {useState} from 'react';
import {Text, View, StyleSheet, Alert} from 'react-native';
import {Image, Input, Button} from 'react-native-elements';
// import Icon from 'react-native-vector-icons/Ionicons';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
const EquationScreen = () => {
  const [sh1, setSh1] = useState(0);
  const [sh2, setSh2] = useState(0);
  const [sh3, setSh3] = useState(0);
  const [result, setResult] = useState(0);
  const [result1, setResult1] = useState(0);
  const [textResult, setTextResult] = useState('');

  const onPressLogo = () => {
    Alert.alert('Xin chào', 'Chúng tôi là Nhóm 4!');
  };

  const checkIsNumber = () => {
    if (!Number(sh1) && Number(sh1) != 0) {
      Alert.alert('', 'Số đầu tiên không phải là số vui lòng nhập lại');
      return false;
    } else if (!Number(sh2) && Number(sh2) != 0) {
      Alert.alert('', 'Số thứ hai không phải là số vui lòng nhập lại');
      return false;
    } else if (!Number(sh3) && Number(sh3) != 0) {
      Alert.alert('', 'Số thứ ba không phải là số vui lòng nhập lại');
      return false;
    } else {
      return true;
    }
  };

  const onCalculate = () => {
    if (checkIsNumber()) {
      var x1, x2;
      var x, y, z, delta;
      x = parseInt(Number(sh1));
      y = parseInt(Number(sh2));
      z = parseInt(Number(sh3));
      delta = y * y - 4 * x * z;
      if (delta == 0) {
        setTextResult('Phương trình có nghiệm kép');
        x1 = -y / (2 * x);
        x2 = -y / (2 * x);
        setResult(eval(x1));
        setResult1(eval(x2));
      } else if (delta < 0) {
        setTextResult('Phương trình vô nghiệm');
        setResult(0);
        setResult1(0);
      } else {
        setTextResult('Phương trình có 2 nghiệm');
        x1 = (-y - Math.sqrt(delta)) / (2 * x);
        x2 = (-y + Math.sqrt(delta)) / (2 * x);
        setResult(eval(x1));
        setResult1(eval(x2));
      }
    } else setResult(0);
  };

  return (
    <View style={styles.Container}>
      <View style={styles.header}>
        <Image
          transitionDuration={500}
          source={require('../imgs/logo_ctu.png')}
          style={{width: 60, height: 60}}
          // onPressLogo
          onPress={onPressLogo}
        />
        <Text style={styles.h_text}>LABO2-ND1-PHẦN TỬ GIAO DIỆN CƠ BẢN</Text>
      </View>
      <View style={styles.input}>
        <Input
          disabledInputStyle={{background: '#ddd'}}
          label="Số hạng 1:"
          // leftIcon={<Icon name="sort-numeric-desc" size={20} />}
          leftIcon={<EvilIcons name="arrow-right" size={40} />}
          placeholder="123"
          color="blue"
          keyboardType="numeric"
          value={sh1}
          onChangeText={sh1 => {
            setSh1(sh1);
          }}
          labelStyle={{color: 'blue'}}
          inputStyle={{color: 'blue', textAlign: 'center'}}
        />
        <Input
          disabledInputStyle={{background: '#ddd'}}
          label="Số hạng 2:"
          // leftIcon={<Icon name="sort-numeric-desc" size={20} />}
          leftIcon={<EvilIcons name="arrow-right" size={40} />}
          placeholder="123"
          keyboardType="numeric"
          value={sh2}
          onChangeText={sh2 => {
            setSh2(sh2);
          }}
          labelStyle={{color: 'blue'}}
          inputStyle={{color: 'blue', textAlign: 'center'}}
        />
        <Input
          disabledInputStyle={{background: '#ddd'}}
          label="Số hạng 3:"
          // leftIcon={<Icon name="sort-numeric-desc" size={20} />}
          leftIcon={<EvilIcons name="arrow-right" size={40} />}
          placeholder="123"
          keyboardType="numeric"
          value={sh3}
          onChangeText={sh3 => {
            setSh3(sh3);
          }}
          labelStyle={{color: 'blue'}}
          inputStyle={{color: 'blue', textAlign: 'center'}}
        />
        <Text style={{fontSize: 24}}>KẾT QUẢ</Text>
        <Text style={{fontSize: 24, color: 'blue'}}>{textResult}</Text>
        <Text style={{fontSize: 24, color: 'blue'}}>nghiệm 1: {result}</Text>
        <Text style={{fontSize: 24, color: 'blue'}}>nghiệm 2: {result1}</Text>
      </View>
      <View style={styles.operator}>
        <Button
          title="Giải phương trình bậc 2"
          containerStyle={{
            width: '90%',
            marginLeft: '5%',
          }}
          onPress={onCalculate}
        />
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  Container: {
    flex: 1,
  },
  header: {
    flex: 0.7,
    alignItems: 'center',
    flexDirection: 'row',
    paddingLeft: 10,
  },
  input: {
    flex: 3,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  operator: {
    flex: 1,
    // backgroundColor: 'lightgrey',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  h_text: {
    flex: 3,
    fontSize: 20,
    textAlign: 'center',
    color: 'blue',
  },
  h_logo: {
    flex: 2,
  },
});

export default EquationScreen;
