/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React, {useState} from 'react';
import {Text, View, StyleSheet, Alert, TouchableOpacity} from 'react-native';
import {Image, Input, Button} from 'react-native-elements';
// import Icon from 'react-native-vector-icons/Ionicons';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import prompt from 'react-native-prompt-android';
const EquationScreenPrompt = () => {
  const [sh, setSh] = useState({sh1: 0, sh2: 0, sh3: 0});
  const [result, setResult] = useState(0);
  const [result1, setResult1] = useState(0);
  const [textResult, setTextResult] = useState('');

  const onPressLogo = () => {
    Alert.alert('Xin chào', 'Chúng tôi là Nhóm 4!');
  };

  const checkIsNumber = () => {
    if (!Number(sh.sh1) && Number(sh.sh1) != 0) {
      Alert.alert('', 'Số đầu tiên không phải là số vui lòng nhập lại');
      return false;
    } else if (!Number(sh.sh2) && Number(sh.sh2) != 0) {
      Alert.alert('', 'Số thứ hai không phải là số vui lòng nhập lại');
      return false;
    } else if (!Number(sh.sh3) && Number(sh.sh3) != 0) {
      Alert.alert('', 'Số thứ ba không phải là số vui lòng nhập lại');
      return false;
    } else {
      return true;
    }
  };

  const sh1 = {
    name: 'sh1',
    title: 'Số hạng 1',
    description: '',
    type: 'numeric',
    defaultValue: sh.sh1,
  };
  const sh2 = {
    name: 'sh2',
    title: 'Số hạng 2',
    description: '',
    type: 'numeric',
    defaultValue: sh.sh2,
  };
  const sh3 = {
    name: 'sh3',
    title: 'Số hạng 3',
    description: '',
    type: 'numeric',
    defaultValue: sh.sh3,
  };

  //prompt theo mẫu https://github.com/shimohq/react-native-prompt-android/blob/master/Example/main.js
  const _prompt = params => {
    console.log(params);
    if (Platform.OS === 'android') {
      prompt(
        params.title,
        params.description,
        [
          {
            text: 'Thoát',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {
            text: 'OK',
            onPress: value => {
              setSh({...sh, [params.name]: value});
              console.log(`OK Pressed, ${params.name}: ${value}`);
            },
          },
        ],
        {
          type: params.type,
          cancelable: false,
          defaultValue: params.defaultValue,
          placeholder: 'Nhập giá trị số hạng',
          style: 'default',
        },
      );
    } else {
      prompt(
        params.title,
        params.description,
        [
          {
            text: 'Thoát',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {
            text: 'OK',
            onPress: value => {
              setSh({...sh, [params.name]: value});
              console.log(`OK Pressed, ${params.name}: ${value}`);
            },
          },
        ],
        {
          type: params.type,
          cancelable: false,
          defaultValue: params.defaultValue,
          placeholder: 'Nhập giá trị số hạng',
        },
      );
    }
  };
  // giải phương trình bậc 2
  const onCalculate = () => {
    if (checkIsNumber()) {
      var x1, x2;
      var x, y, z, delta;
      x = parseInt(Number(sh.sh1));
      y = parseInt(Number(sh.sh2));
      z = parseInt(Number(sh.sh3));
      delta = y * y - 4 * x * z;
      if (delta == 0) {
        setTextResult('Phương trình có nghiệm kép');
        x1 = -y / (2 * x);
        x2 = -y / (2 * x);
        setResult(eval(x1));
        setResult1(eval(x2));
      } else if (delta < 0) {
        setTextResult('Phương trình vô nghiệm');
        setResult(0);
        setResult1(0);
      } else {
        setTextResult('Phương trình có 2 nghiệm');
        x1 = (-y - Math.sqrt(delta)) / (2 * x);
        x2 = (-y + Math.sqrt(delta)) / (2 * x);
        setResult(eval(x1));
        setResult1(eval(x2));
      }
    } else setResult(0);
  };

  return (
    <View style={styles.Container}>
      <View style={styles.header}>
        <Image
          transitionDuration={500}
          source={require('../imgs/logo_ctu.png')}
          style={{width: 60, height: 60}}
          // onPressLogo
          onPress={onPressLogo}
        />
        <Text style={styles.h_text}>LABO2-ND1-PHẦN TỬ GIAO DIỆN CƠ BẢN</Text>
      </View>
      <View style={styles.input}>
        <TouchableOpacity style={styles.button} onPress={() => _prompt(sh1)}>
          <Text style={styles.buttonText}>Số hạng thứ nhất</Text>
          <Text style={styles.buttonText}>{sh.sh1}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => _prompt(sh2)}>
          <Text style={styles.buttonText}>Số hạng thứ hai</Text>
          <Text style={styles.buttonText}>{sh.sh2}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => _prompt(sh3)}>
          <Text style={styles.buttonText}>Số hạng thứ ba</Text>
          <Text style={styles.buttonText}>{sh.sh3}</Text>
        </TouchableOpacity>
        <Text style={{fontSize: 24}}>KẾT QUẢ</Text>
        <Text style={{fontSize: 24, color: 'blue'}}>{textResult}</Text>
        <Text style={{fontSize: 24, color: 'blue'}}>nghiệm 1: {result}</Text>
        <Text style={{fontSize: 24, color: 'blue'}}>nghiệm 2: {result1}</Text>
      </View>
      <View style={styles.operator}>
        <Button
          title="Giải phương trình bậc 2"
          containerStyle={{
            width: '90%',
            marginLeft: '5%',
          }}
          onPress={onCalculate}
        />
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  Container: {
    flex: 1,
  },
  header: {
    flex: 0.7,
    alignItems: 'center',
    flexDirection: 'row',
    paddingLeft: 10,
  },
  input: {
    flex: 3,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  operator: {
    flex: 1,
    // backgroundColor: 'lightgrey',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  h_text: {
    flex: 3,
    fontSize: 20,
    textAlign: 'center',
    color: 'blue',
  },
  h_logo: {
    flex: 2,
  },
  button: {
    borderRadius: 5,
    borderBottomWidth: 1,
    paddingVertical: 5,
    paddingHorizontal: 16,
    height: 50,
    width: '100%',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  buttonContent: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  buttonText: {
    textAlign: 'center',
    color: 'blue',
    marginBottom: 5,
    fontSize: 18,
    fontWeight: '700',
  },
});

export default EquationScreenPrompt;
