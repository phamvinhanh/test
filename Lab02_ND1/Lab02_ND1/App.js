/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React, {useState} from 'react';
import {Alert, StyleSheet, Text, View} from 'react-native';
import {Image, Input, Button} from 'react-native-elements';
//e, thay dổi icon
// import Icon from 'react-native-vector-icons/FontAwesome';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
const App = () => {
  const [sh1, setSh1] = useState(0);
  const [sh2, setSh2] = useState(0);
  const [result, setResult] = useState(0);
  //a, khi ấn vào logo

  const onPressLogo = () => {
    Alert.alert('Xin chào', 'Chúng tôi là Nhóm 4!');
  };

  //c, check  sh1 , sh2 không phải là số

  const checkIsNumber = () => {
    if (!Number(sh1) && Number(sh1) != 0) {
      Alert.alert('', 'Số đầu tiên không phải là số vui lòng nhập lại');
      return false;
    } else if (!Number(sh2) && Number(sh2) != 0) {
      Alert.alert('', 'Số thứ hai không phải là số vui lòng nhập lại');
      return false;
    } else {
      return true;
    }
  };

  //d, chia cho 0

  const checkIsNumberOther = () => {
    if (Number(sh2) == 0) {
      Alert.alert('', 'Số chia phải khác 0, vui lòng nhập lại');
      return false;
    } else {
      return true;
    }
  };

  //b, tính toán

  const onCalculate = sign => {
    if (checkIsNumber()) {
      switch (sign) {
        case '+':
          setResult(Number(sh1) + Number(sh2));
          break;
        case '-':
          setResult(Number(sh1) - Number(sh2));
          break;
        case '*':
          setResult(Number(sh1) * Number(sh2));
          break;
        case '/':
          if (checkIsNumberOther()) {
            setResult(Number(sh1) / Number(sh2));
          }
          break;
      }
    } else setResult(0);
  };

  return (
    <View style={styles.Container}>
      <View style={styles.header}>
        <Image
          transitionDuration={500}
          source={require('./imgs/logo_ctu.png')}
          style={{width: 60, height: 60}}
          // onPressLogo
          onPress={onPressLogo}
        />
        <Text style={styles.h_text}>LABO2-ND1-PHẦN TỬ GIAO DIỆN CƠ BẢN</Text>
      </View>
      <View style={styles.input}>
        <Input
          disabledInputStyle={{background: '#ddd'}}
          label="Số hạng 1:"
          //e,thay dổi icon
          // leftIcon={<Icon name="sort-numeric-desc" size={20} />}
          leftIcon={<EvilIcons name="arrow-right" size={40} />}
          placeholder="123"
          color="blue"
          keyboardType="numeric"
          value={sh1}
          onChangeText={sh1 => {
            setSh1(sh1);
          }}
          labelStyle={{color: 'blue'}}
          inputStyle={{color: 'blue', textAlign: 'center'}}
        />
        <Input
          disabledInputStyle={{background: '#ddd'}}
          label="Số hạng 2:"
          //e,thay dổi icon
          // leftIcon={<Icon name="sort-numeric-desc" size={20} />}
          leftIcon={<EvilIcons name="arrow-right" size={40} />}
          placeholder="123"
          keyboardType="numeric"
          value={sh2}
          onChangeText={sh2 => {
            setSh2(sh2);
          }}
          labelStyle={{color: 'blue'}}
          inputStyle={{color: 'blue', textAlign: 'center'}}
        />
        <Text style={{fontSize: 24}}>KẾT QUẢ</Text>
        <Text style={{fontSize: 32, color: 'blue'}}>{result}</Text>
      </View>
      <View style={styles.operator}>
        <Button
          title="+"
          containerStyle={{
            width: '24%',
            marginLeft: 1,
          }}
          onPress={() => {
            onCalculate('+');
          }}
        />
        <Button
          title="-"
          containerStyle={{
            width: '24%',
          }}
          onPress={() => {
            onCalculate('-');
          }}
        />
        <Button
          title="*"
          containerStyle={{
            width: '24%',
          }}
          onPress={() => {
            onCalculate('*');
          }}
        />
        <Button
          title="/"
          containerStyle={{
            width: '24%',
            marginRight: 1,
          }}
          onPress={() => {
            onCalculate('/');
          }}
        />
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  Container: {
    flex: 1,
  },
  header: {
    flex: 0.7,
    alignItems: 'center',
    flexDirection: 'row',
    paddingLeft: 10,
  },
  input: {
    flex: 3,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  operator: {
    flex: 1,
    // backgroundColor: 'lightgrey',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  h_text: {
    flex: 3,
    fontSize: 20,
    textAlign: 'center',
    color: 'blue',
  },
  h_logo: {
    flex: 2,
  },
});
export default App;
