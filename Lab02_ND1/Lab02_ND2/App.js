/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {Image, Input, Button} from 'react-native-elements';
const App = () => {
  const DATA = [
    {
      id: 1,
      path: require('../imgs/nam.png'),
      hoten: 'Nguyễn Khôi Nguyên',
      lop: ' Kỹ thuật máy tính',
      khoa: 'Công nghệ',
      sothich: 'hát',
      sotruong: 'chơi game',
    },
    {
      id: 2,
      path: require('../imgs/nam.png'),
      hoten: 'Lê Minh Trường',
      lop: 'Kỹ thuật máy tính',
      khoa: 'Công nghẹ',
      sothich: 'thích đi du lịch',
      sotruong: 'chơi game',
    },
    {
      id: 3,
      path: require('../imgs/nam.png'),
      hoten: 'Trần Hoàng Khang',
      lop: ' Kỹ thuật máy tính',
      khoa: 'Công nghệ',
      sothich: 'hát',
      sotruong: 'chơi game',
    },
    {
      id: 4,
      path: require('../imgs/nam.png'),
      hoten: 'Huỳnh Thanh Hạo',
      lop: ' Kỹ thuật máy tính',
      khoa: 'Công nghệ',
      sothich: 'du lịch',
      sotruong: 'chơi game',
    },
    {
      id: 5,
      path: require('../imgs/nam.png'),
      hoten: 'Lư Tất Thắng',
      lop: ' Kỹ thuật máy tính',
      khoa: 'Công nghệ',
      sothich: 'học',
      sotruong: 'chơi game',
    },
  ];
  //a, giới thiệu sinh viên

  const onIntroduce = item => {
    Alert.alert('Giới thiệu', `Xin chào tôi là ${item?.hoten}`);
  };

  const Item = ({item, backgroundColor}) => (
    <TouchableOpacity
      //a, tạo nút bấm vào xem giới thiệu
      onPress={() => onIntroduce(item)}
      style={[styles.flatlist, backgroundColor]}>
      <Image
        transitionDuration={500}
        source={item.path}
        style={{width: 60, height: 60}}
      />
      <View style={{flex: 1}}>
        <Text style={{flex: 1, marginLeft: 20}}>Họ và tên: {item.hoten}</Text>
        <Text style={{flex: 1, marginLeft: 20}}>Lớp: {item.lop}</Text>
        <Text style={{flex: 1, marginLeft: 20}}>Khoa: {item.khoa}</Text>
        {/* c, thêm thong tin của sinh viên trong nhóm */}
        <Text style={{flex: 1, marginLeft: 20}}>Sở thích: {item.sothich}</Text>
        <Text style={{flex: 1, marginLeft: 20}}>
          Sở trường: {item?.sotruong}
        </Text>
      </View>
    </TouchableOpacity>
  );
  const renderItem = ({item, index}) => {
    //d, hiệu chỉnh thay đổi đổi backgroundColor chắn lẻ theo danh sách
    const backgroundColor = index % 2 == 0 ? 'lightblue' : 'lightgray';
    return <Item item={item} backgroundColor={{backgroundColor}} />;
  };
  return (
    <View style={styles.Container}>
      <View style={styles.header}>
        <Image
          transitionDuration={500}
          source={require('./imgs/logo_ctu.png')}
          style={{width: 60, height: 60}}
          onPress={() => alert('Xin chào, Chúng tôi là Nhóm 1!')}
        />
        <Text style={styles.h_text}>LABO2-ND2-PHẦN TỬ GIAO DIỆN CƠ BẢN</Text>
      </View>
      <View style={styles.l_view}>
        <FlatList
          data={DATA}
          renderItem={renderItem}
          keyExtractor={item => item.id}
          extraData={DATA}
        />
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  Container: {
    flex: 1,
  },
  header: {
    flex: 0.7,
    alignItems: 'center',
    flexDirection: 'row',
    paddingLeft: 10,
    backgroundColor: 'lightgray',
    borderRadius: 10,
  },
  l_view: {
    flex: 4,
    alignItems: 'flex-start',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  h_text: {
    flex: 1,
    fontSize: 20,
    textAlign: 'center',
    color: 'blue',
  },
  h_logo: {
    flex: 1,
  },
  l_image: {
    flex: 1,
  },
  t_text: {
    flex: 1,
  },
  flatlist: {
    flex: 1,
    fontSize: 20,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    color: 'blue',
    padding: 10,
    backgroundColor: 'lightblue',
    marginTop: 3,
    borderRadius: 10,
  },
});
export default App;
