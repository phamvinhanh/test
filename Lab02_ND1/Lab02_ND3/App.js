/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React, {useState} from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {NavigationContainer} from '@react-navigation/native';
import CalculationScreen from './screens/CalculationScreen';
import EquationScreen from './screens/EquationScreen';
import AboutScreen from './screens/AboutScreen';
import EquationScreenPrompt from './screens/EquationScreenPrompt';

const Tab = createBottomTabNavigator();
const App = () => {
  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({route}) => ({
          tabBarIcon: ({focused, color, size}) => {
            let iconName;
            if (route.name === 'Calculation') {
              iconName = focused
                ? 'information-circle'
                : 'information-circle-outline';
            } else if (route.name === 'About') {
              iconName = focused ? 'list-circle-outline' : 'list-circle';
            } else if (route.name === 'Equation') {
              iconName = focused ? 'remove-circle-outline' : 'remove-circle';
            }
            // You can return any component that you like here!
            return <Icon name={iconName} size={size} color={color} />;
          },
          tabBarActiveTintColor: 'tomato',
          tabBarInactiveTintColor: 'gray',
        })}>
        <Tab.Screen name="Calculation" component={CalculationScreen} />
        {/* <Tab.Screen name="Equation" component={EquationScreen} /> */}
        <Tab.Screen name="Equation" component={EquationScreenPrompt} />
        <Tab.Screen name="About" component={AboutScreen} />
      </Tab.Navigator>
    </NavigationContainer>
  );
};
export default App;
